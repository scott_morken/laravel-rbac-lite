<?php
return [
    'admin' => [
        'allow' => [
            /*
             * Can be a list of action names (getIndex) or * for all actions
             */
            'actions' => ['*'],
            /*
             * Can be a list of ids, role_names or * (all roles)
             */
            'roles'   => ['admin'],
            /*
             * Can be a list of ids or one of *, @, ?
             * * - all users
             * @ - authenticated users
             * ? - guest users
             */
            'users'   => ['user_id1', 'user_id2'],
        ],
        'deny'  => [
            'roles' => ['*'],
            'user'  => ['*'],
        ],
    ],
];
