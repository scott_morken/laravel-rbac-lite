<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 12:35 PM
 */

use Smorken\Rbac\Models\Eloquent\Role;

class RoleTableSeeder extends \Illuminate\Database\Seeder
{

    public function run()
    {
        Role::truncate();
        Role::create(
            [
                'role_name'   => 'super_admin',
                'description' => 'Super Admin',
                'super_admin' => true,
            ]
        );
        Role::create(
            [
                'role_name'    => 'admin',
                'description'  => 'Admin',
                'inherit_from' => 'manage',
            ]
        );
        Role::create(
            [
                'role_name'    => 'manage',
                'description'  => 'Manager',
                'inherit_from' => 'user',
            ]
        );
        Role::create(
            [
                'role_name'   => 'user',
                'description' => 'User',
            ]
        );
    }
}
