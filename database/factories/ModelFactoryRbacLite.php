<?php
$factory->define(
    Smorken\Rbac\Models\Eloquent\Role::class,
    function (Faker\Generator $faker) {
        return [
            'role_name'    => str_random(10),
            'description'  => $faker->words(3, true),
            'inherit_from' => null,
            'super_admin'  => 0,
        ];
    }
);

$factory->define(
    Smorken\Rbac\Models\Eloquent\RoleUser::class,
    function (Faker\Generator $faker) {
        return [
            'role_id'    => rand(1, 3),
            'user_id'  => $faker->randomNumber(2),
        ];
    }
);
