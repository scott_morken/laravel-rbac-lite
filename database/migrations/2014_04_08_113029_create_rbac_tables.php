<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRbacTables extends Migration
{

    protected $tables = [
        'roles',
        'role_user',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->roles();
        $this->roleUser();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::drop($table);
        }
    }

    protected function roles()
    {
        Schema::create(
            'roles',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('role_name', 16);
                $table->string('description', 64);
                $table->string('inherit_from', 16)->nullable();
                $table->boolean('super_admin')->default(false);
                $table->timestamps();

                $table->index('inherit_from', 'role_inherit_from_ndx');
                $table->unique('role_name', 'role_role_name_ndx');
                $table->index('super_admin', 'role_super_admin_ndx');
            }
        );
    }

    protected function roleUser()
    {
        Schema::create(
            'role_user',
            function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();
                $table->timestamps();

                $table->index('user_id', 'ruser_user_id_ndx');
                $table->index('role_id', 'ruser_role_id_ndx');
            }
        );
    }
}
