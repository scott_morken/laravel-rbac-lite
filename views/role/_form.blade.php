{!! Form::model($model, ['action' => [$controller . '@postSave', $model->id]]) !!}
<div class="form-group">
    {!! Form::label('role_name', 'Role Name') !!}
    {!! Form::text('role_name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('inherit_from', 'Inherit From') !!}
    {!! Form::text('inherit_from', null, ['class' => 'form-control']) !!}
</div>
<div class="checkbox">
    <input type="hidden" name="super_admin" value="0">
    <label for="superadmin-checked">
        {!! Form::checkbox('super_admin', 1, ['class' => 'checkbox', 'id' => 'superadmin-checked']) !!}
        Super Admin Role
    </label>
</div>
{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
{!! HTML::linkAction($controller . '@getIndex', 'Cancel', [], ['class' => 'btn btn-warning']) !!}
{!! Form::close() !!}
