@extends($master)
@section('content')
    <h4>Update role #{{$model->id}}</h4>
    @include('smorken/rbac::role._form')
@stop
