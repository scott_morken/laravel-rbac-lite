@extends($master)
@section('content')
    <h4>Create new role</h4>
    @include('smorken/rbac::role._form')
@stop
