@extends($master)
@section('content')
    <h4>Roles</h4>
    {!! HTML::tableView(
        $models,
        [
            'id' => 'ID',
            'role_name' => 'Name',
            'description' => 'Description',
            'inherit_from' => 'Inherits',
            'super_admin' => [
                'label' => 'Super Admin',
                'value' => function ($m)
                {
                    return $m->super_admin == 1 ? 'Yes' : 'No';
                },
            ],
        ],
        ['class' => 'table-striped'],
        $controller
    ) !!}
@stop
