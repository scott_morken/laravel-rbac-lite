@extends($master)
@section('content')
    <h4>Role #{{ $model->id }}</h4>
    {!! HTML::detailView($model, [
        'id' => 'ID',
        'role_name' => 'Role',
        'description' => 'Description',
        'inherit_from' => 'Inherits',
        'super_admin' => [
            'label' => 'Super Admin',
            'value' => $model->super_admin == 1 ? 'Yes' : 'No',
        ],
    ]) !!}
@stop
