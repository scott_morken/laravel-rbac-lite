## Laravel 5 RBAC Lite package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 5.5+
* [Composer](https://getcomposer.org/)

#### Installation

* add to your Laravel app composer.json

```
"require": {
    "smorken/rbac-lite": "~5.0"
}
```

* `composer update`

* add service provider to `config/app.php`

```
'providers' => [
...
    \Smorken\Rbac\ServiceProvider::class,
```

* publish the needed files

```
$ php artisan vendor:publish --provider="Smorken\Rbac\ServiceProvider" --tag=db #view and config also available
```

* run the migrations (might need to dump-autoload again)

```
$ php artisan db:seed --class="RbacSeeder"
```

#### Use

`app('rbac')` provides an instance of `Smorken\Rbac\Handler`

`rbac()` is a shortcut for the same

Checking a user's roles:

```
$rbac = app('rbac');
if ($rbac->hasRole(1)) ...

if ($rbac->hasRole('admin')) ...

if (rbac(1)) ...

if (rbac('admin')) ...
```

#### Rules

Rules can be set either as middleware in the routes or directly on the controller in the docblocks

_Middleware_ (`routes.php`)

```
Route::group(
    [
        'prefix'     => 'admin',
        'middleware' => ['auth', 'rbac'],
        'namespace'  => 'Admin',
        'rbac'       => [
            'allow' => [
                'actions' => ['*'],
                'roles'   => ['admin'],
                'users'   => ['user_id1', 'user_id2'],
            ],
            'deny'  => [
                'roles' => ['*'],
                'user'  => ['*'],
            ],
        ],
    ],
```

Using a config file (eg. `config/rbac.php`) with routes middleware

```
Route::group(
    [
        'prefix'     => 'admin',
        'middleware' => ['auth', 'rbac'],
        'namespace'  => 'Admin',
        'rbac'       => config('rbac.admin'),
    ],
```

_Controller_

```
/**
 * @rbac allow|roles:admin,manage
 * @rbac deny|users:99
 */
class MyController extends Controller
    
    /**
     * @rbac allow|users:99
     */
    public function getView() { } //will override controller level
    
    public function getAdmin() { } //will use controller level

```
