<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 3:08 PM
 */
include_once 'ModelBase.php';

use Smorken\Rbac\Models\Eloquent\Role;
use Mockery as m;

class RoleModelTest extends \PHPUnit\Framework\TestCase
{

    use ModelTrait;

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testSut()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with('select * from "roles" where "role_name" = ? limit 1')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 'admin', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn([['id' => 1, 'role_name' => 'admin', 'description' => 'foo']]);
        $r = $sut->where('role_name', 'admin')->first();
        $this->assertInstanceOf(\Smorken\Rbac\Models\Eloquent\Role::class, $r);
        $this->assertEquals('foo', $r->description);
    }

    public function testRoleNameContainsAlphaNumDash()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $sut->role_name = 'Hello, my name is #FOO';
        $this->assertEquals('hello-my-name-is-foo', $sut->role_name);
    }

    public function testInheritsFromWithNoInheritance()
    {
        list($sut, $pdo) = $this->getSutAndMocks(
            ['id' => 2, 'role_name' => 'foo', 'description' => 'fiz role', 'inherit_from' => null]
        );
        $inh = $sut->inheritsFrom();
        $this->assertCount(1, $inh);
        $role = $inh->get(2);
        $this->assertEquals('foo', $role->role_name);
    }

    public function testInheritsSuperAdmin()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $pdo) = $this->getSutAndMocks($inherits['super-admin']);
        unset($inherits['super-admin']);
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with('select * from "roles" where "super_admin" = ?')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '0', 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($inherits);
        $inh = $sut->inheritsFrom();
        $inherited = 0;
        foreach ($inh as $m) {
            if ($m->inherited) {
                $inherited++;
            }
        }
        $this->assertCount(4, $inh);
        $this->assertEquals(3, $inherited);
    }

    protected function getInheritsArray()
    {
        return [
            'super-admin' => [
                'id'           => 1,
                'role_name'    => 'super-admin',
                'description'  => 'super role',
                'inherit_from' => null,
                'super_admin'  => 1,
            ],
            'admin'       => [
                'id'           => 2,
                'role_name'    => 'admin',
                'description'  => 'Admin',
                'inherit_from' => 'manage',
            ],
            'manage'      => [
                'id'           => 3,
                'role_name'    => 'manage',
                'description'  => 'Manage',
                'inherit_from' => 'user',
            ],
            'user'        => [
                'id'           => 4,
                'role_name'    => 'user',
                'description'  => 'User',
                'inherit_from' => null,
            ],
        ];
    }

    public function testInheritsFromAdmin()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        list($sut, $pdo) = $this->getSutAndMocks($inherits['admin']);
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with('select * from "roles" where "role_name" = ? limit 1')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->once()->with(1, 'manage', 2);
        $stmt->shouldReceive('bindValue')->once()->with(1, 'user', 2);
        $stmt->shouldReceive('execute')->twice()->with();
        $stmt->shouldReceive('fetchAll')->andReturn([$inherits['manage']], [$inherits['user']]);
        $inh = $sut->inheritsFrom();
        $inherited = 0;
        foreach ($inh as $m) {
            if ($m->inherited) {
                $inherited++;
            }
        }
        $this->assertCount(3, $inh);
        $this->assertEquals(2, $inherited);
    }

    public function testInheritsFromNonInheritable()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        list($sut, $pdo) = $this->getSutAndMocks($inherits['user']);
        $pdo->shouldNotReceive('prepare');
        $inh = $sut->inheritsFrom();
        $model = $inh->first();
        $this->assertCount(1, $inh);
        $this->assertFalse($model->inherited);
    }

    protected function getModelClass()
    {
        return Role::class;
    }
}
