<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 10:10 AM
 */
include_once 'ModelBase.php';

use Mockery as m;

class RoleUserModelTest extends \PHPUnit\Framework\TestCase
{

    use ModelTrait;

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testSut()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with('select * from "role_user" where "role_id" = ? limit 1')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 1, 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn([['id' => 1, 'role_id' => 1, 'user_id' => 1]]);
        $r = $sut->where('role_id', 1)->first();
        $this->assertInstanceOf(\Smorken\Rbac\Models\Eloquent\RoleUser::class, $r);
        $this->assertEquals('1', $r->user_id);
    }

    public function testAddExists()
    {
        $results = [
            ['id' => 1, 'user_id' => 1, 'role_id' => 1],
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where ("user_id" = ? and "role_id" = ?) limit 1'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $stmt->shouldReceive('bindValue')->with(2, '1', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($results);
        $model = $sut->add('1', '1');
        $this->assertEquals(1, $model->id);
    }

    public function testAddNotExists()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where ("user_id" = ? and "role_id" = ?) limit 1'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->withArgs(function ($k, $v, $type) {
            return $v === '1' || strpos($v, '-') !== false;
        });
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn([]);
        $pdo->shouldReceive('prepare')->with(
            'insert into "role_user" ("user_id", "role_id", "updated_at", "created_at") values (?, ?, ?, ?)'
        )
            ->andReturn($stmt);
        $pdo->shouldReceive('lastInsertId')->andReturn(1);
        $model = $sut->add('1', '1');
        $this->assertEquals(1, $model->id);
    }

    public function testRemove()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,rowCount,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'delete from "role_user" where "user_id" = ? and "role_id" = ?'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $stmt->shouldReceive('bindValue')->with(2, '1', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('rowCount')->andReturn(1);
        $this->assertEquals(1, $sut->remove('1', '1'));
    }

    public function testRemoveWithRoleName()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,rowCount,bindValue]');
        $pdo->shouldReceive('prepare')
            ->with(
                'delete from "role_user" where "user_id" = ? and exists (select * from "roles" where "role_user"."role_id" = "roles"."id" and "role_name" = ?)'
            )
            ->andReturn(
                $stmt
            );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $stmt->shouldReceive('bindValue')->with(2, 'admin', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('rowCount')->andReturn(1);
        $this->assertEquals(1, $sut->remove('1', 'admin'));
    }

    public function testRemoveByUserId()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,rowCount,bindValue]');
        $pdo->shouldReceive('prepare')->with('delete from "role_user" where "user_id" = ?')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('rowCount')->andReturn(2);
        $this->assertEquals(2, $sut->removeByUserId('1'));
    }

    public function testRemoveByRoleId()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,rowCount,bindValue]');
        $pdo->shouldReceive('prepare')->with('delete from "role_user" where "role_id" = ?')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('rowCount')->andReturn(2);
        $this->assertEquals(2, $sut->removeByRoleId('1'));
    }

    public function testRemoveByRoleIdWithRoleName()
    {
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,rowCount,bindValue]');
        $pdo->shouldReceive('prepare')
            ->with(
                'delete from "role_user" where exists (select * from "roles" where "role_user"."role_id" = "roles"."id" and "role_name" = ?)'
            )
            ->andReturn(
                $stmt
            );
        $stmt->shouldReceive('bindValue')->with(1, 'admin', 2);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('rowCount')->andReturn(2);
        $this->assertEquals(2, $sut->removeByRoleId('admin'));
    }

    public function testHasRole()
    {
        $results = [
            ['id' => 1, 'user_id' => 1, 'role_id' => 1],
        ];
        $roles = [
            ['id' => 1, 'role_name' => 'admin', 'description' => 'Admin Role'],
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where "role_id" = ? and "user_id" = ? limit 1'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $stmt->shouldReceive('bindValue')->with(2, '1', 2);
        $pdo->shouldReceive('prepare')->with(
            'select * from "roles" where "roles"."id" in (?)'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 1, 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($results, $roles);
        $model = $sut->hasRole('1', '1');
        $this->assertEquals(1, $model->id);
        $this->assertEquals('admin', $model->role->role_name);
    }

    public function testHasRoleWithRoleName()
    {
        $results = [
            ['id' => 1, 'user_id' => 1, 'role_id' => 1],
        ];
        $roles = [
            ['id' => 1, 'role_name' => 'admin', 'description' => 'Admin Role'],
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where exists (select * from "roles" where "role_user"."role_id" = "roles"."id" and "role_name" = ?) and "user_id" = ? limit 1'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 'admin', 2);
        $stmt->shouldReceive('bindValue')->with(2, '1', 2);
        $pdo->shouldReceive('prepare')->with(
            'select * from "roles" where "roles"."id" in (?)'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 1, 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($results, $roles);
        $model = $sut->hasRole('1', 'admin');
        $this->assertEquals(1, $model->id);
        $this->assertEquals('admin', $model->role->role_name);
    }

    public function testHasRoles()
    {
        $results = [
            ['id' => 1, 'user_id' => 1, 'role_id' => 1],
        ];
        $roles = [
            ['id' => 1, 'role_name' => 'admin', 'description' => 'Admin Role'],
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where "user_id" = ?'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $pdo->shouldReceive('prepare')->with(
            'select * from "roles" where "roles"."id" in (?)'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 1, 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($results, $roles);
        $models = $sut->hasRoles('1');
        $this->assertCount(1, $models);
        $model = $models->first();
        $this->assertEquals(1, $model->id);
        $this->assertEquals('admin', $model->role->role_name);
    }

    public function testHasUsers()
    {
        $results = [
            ['id' => 1, 'user_id' => 1, 'role_id' => 1],
        ];
        $roles = [
            ['id' => 1, 'role_name' => 'admin', 'description' => 'Admin Role'],
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where "role_id" = ?'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, '1', 2);
        $pdo->shouldReceive('prepare')->with(
            'select * from "roles" where "roles"."id" in (?)'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 1, 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($results, $roles);
        $models = $sut->hasUsers('1');
        $this->assertCount(1, $models);
        $model = $models->first();
        $this->assertEquals(1, $model->id);
        $this->assertEquals('admin', $model->role->role_name);
    }

    public function testHasUsersWithRoleName()
    {
        $results = [
            ['id' => 1, 'user_id' => 1, 'role_id' => 1],
        ];
        $roles = [
            ['id' => 1, 'role_name' => 'admin', 'description' => 'Admin Role'],
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $stmt = m::mock('\PDOStatement[execute,fetchAll,bindValue]');
        $pdo->shouldReceive('prepare')->with(
            'select * from "role_user" where exists (select * from "roles" where "role_user"."role_id" = "roles"."id" and "role_name" = ?)'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 'admin', 2);
        $pdo->shouldReceive('prepare')->with(
            'select * from "roles" where "roles"."id" in (?)'
        )->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->with(1, 1, 1);
        $stmt->shouldReceive('execute')->with();
        $stmt->shouldReceive('fetchAll')->andReturn($results, $roles);
        $models = $sut->hasUsers('admin');
        $this->assertCount(1, $models);
        $model = $models->first();
        $this->assertEquals(1, $model->id);
        $this->assertEquals('admin', $model->role->role_name);
    }

    protected function getModelClass()
    {
        return \Smorken\Rbac\Models\Eloquent\RoleUser::class;
    }
}
