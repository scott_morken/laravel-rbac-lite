<?php

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 11:15 AM
 */
class Cache
{

    public static function remember($key, $time, $func)
    {
        return $func();
    }

    public static function flush()
    {
        return true;
    }
}
