<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 1:09 PM
 */
use Mockery as m;

class RoleCommandTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testHandleWithUserId()
    {
        list($sut, $app, $rp, $rup, $ctnr) = $this->getSutAndMocks();
        $role = new \Smorken\Rbac\Models\Eloquent\Role(['role_name' => 'foo', 'description' => 'foo bar']);
        $rp->shouldReceive('getSuperAdminRole')->andReturn($role);
        $rup->shouldReceive('add')->with(1, $role->id)->andReturn(true);
        $rup->shouldReceive('errors')->andReturn(false);
        $cmd = $app->find('rbac:role');
        $tester = new \Symfony\Component\Console\Tester\CommandTester($cmd);
        $tester->execute(
            [
                'command' => 'rbac:role',
                'user_id' => 1,
            ]
        );
        $this->assertContains('User [1] added to role [foo] foo bar', $tester->getDisplay());
    }

    protected function getSutAndMocks()
    {
        $rup = m::mock('Smorken\Rbac\Contracts\Storage\RoleUser');
        $rp = m::mock('Smorken\Rbac\Contracts\Storage\Role');
        $app = m::mock('Illuminate\Contracts\Foundation\Application');
        $events = m::mock('Illuminate\Contracts\Events\Dispatcher');
        $events->shouldReceive('dispatch');
        $sapp = new \Illuminate\Console\Application($app, $events, '5.4.0');
        $sut = new \Smorken\Rbac\Console\Commands\Role($rup, $rp);
        $sut->setLaravel($app);
        $sapp->add($sut);
        $app->shouldReceive('call')->with([$sut, 'handle'])->andReturnUsing(
            function () use ($sut) {
                return $sut->handle();
            }
        );
        return [$sut, $sapp, $rp, $rup, $app];
    }

    public function testHandleWithRoleId()
    {
        list($sut, $app, $rp, $rup, $ctnr) = $this->getSutAndMocks();
        $role = new \Smorken\Rbac\Models\Eloquent\Role(['role_name' => 'foo', 'description' => 'foo bar']);
        $rp->shouldReceive('find')->with(2)->andReturn($role);
        $rup->shouldReceive('add')->with(1, $role->id)->andReturn(true);
        $rup->shouldReceive('errors')->andReturn(false);
        $cmd = $app->find('rbac:role');
        $tester = new \Symfony\Component\Console\Tester\CommandTester($cmd);
        $tester->execute(
            [
                'command' => 'rbac:role',
                'user_id' => 1,
                'role_id' => 2,
            ]
        );
        $this->assertContains('User [1] added to role [foo] foo bar', $tester->getDisplay());
    }

    public function testHandleWithErrors()
    {
        list($sut, $app, $rp, $rup, $ctnr) = $this->getSutAndMocks();
        $role = new \Smorken\Rbac\Models\Eloquent\Role(['role_name' => 'foo', 'description' => 'foo bar']);
        $rp->shouldReceive('getSuperAdminRole')->andReturn($role);
        $rup->shouldReceive('add')->with(1, $role->id)->andReturn(true);
        $msgbag = m::mock('MessageBag');
        $msgbag->shouldReceive('all')->andReturn(['error' => 'Foo error']);
        $rup->shouldReceive('errors')->andReturn($msgbag);
        $cmd = $app->find('rbac:role');
        $tester = new \Symfony\Component\Console\Tester\CommandTester($cmd);
        $tester->execute(
            [
                'command' => 'rbac:role',
                'user_id' => 1,
            ]
        );
        $this->assertContains('Unable to add user to role.' . PHP_EOL . 'Foo error', $tester->getDisplay());
    }
}
