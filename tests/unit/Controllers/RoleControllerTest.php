<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 9:14 AM
 */
namespace {

    use Illuminate\Http\Request;
    use Mockery as m;
    use Smorken\Rbac\Http\Requests\RoleForm;

    include_once __DIR__ . '/../helpers.php';

    class RoleControllerTest extends \PHPUnit\Framework\TestCase
    {

        public function tearDown()
        {
            m::close();
        }

        public function testPostSaveWithNoId()
        {
            $input = [
                'role_name' => 'foo',
                'description' => 'Foo',
                'inherit_from' => '',
                'super_admin' => 0
            ];
            list($sut, $role) = $this->getSutAndMocks();
            $s = session();
            $r = redirect();
            $rf = app(RoleForm::class);
            $model = m::mock('Role');
            $request = m::mock(Request::class);
            $request->shouldReceive('all')->andReturn($input);
            $role->shouldReceive('create')->with($input)->andReturn($model);
            $role->shouldReceive('setModel')->with($model);
            $role->shouldReceive('getModel')->andReturn($model);
            $role->shouldReceive('errors')->andReturn(false);
            $role->shouldReceive('name')->with($model)->andReturn('foo');
            $s->shouldReceive('flash')->with('success', 'foo saved.');
            $r->shouldReceive('action')->with('\Smorken\Rbac\Http\Controllers\RoleController@index')->andReturn('redirected');
            $this->assertEquals('redirected', $sut->postSave($request));
        }

        public function testPostSaveWithId()
        {
            $input = [
                'role_name' => 'foo',
                'description' => 'Foo',
                'inherit_from' => '',
                'super_admin' => 0
            ];
            list($sut, $role) = $this->getSutAndMocks();
            $s = session();
            $r = redirect();
            $rf = app(RoleForm::class);
            $model = m::mock('Role');
            $request = m::mock(Request::class);
            $request->shouldReceive('all')->andReturn($input);
            $role->shouldReceive('find')->with(1)->andReturn($model);
            $role->shouldReceive('update')->with($model, $input)->andReturn($model);
            $role->shouldReceive('setModel')->with($model);
            $role->shouldReceive('getModel')->andReturn($model);
            $role->shouldReceive('errors')->andReturn(false);
            $role->shouldReceive('name')->with($model)->andReturn('foo');
            $s->shouldReceive('flash')->with('success', 'foo saved.');
            $r->shouldReceive('action')->with('\Smorken\Rbac\Http\Controllers\RoleController@index')->andReturn('redirected');
            $this->assertEquals('redirected', $sut->postSave($request, 1));
        }

        protected function getSutAndMocks()
        {
            $role = m::mock('Smorken\Rbac\Contracts\Storage\Role');
            $v = view();
            $v->shouldReceive('share')->with('master', 'smorken/views::layouts.rightops');
            $v->shouldReceive('share')->with('sub', 'admin');
            $v->shouldReceive('share')->with('controller', 'Smorken\Rbac\Http\Controllers\RoleController');
            $sut = new \Smorken\Rbac\Http\Controllers\RoleController($role);
            return [$sut, $role];
        }
    }
}

namespace Illuminate\Foundation\Http {
    class FormRequest
    {

    }
}
