<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 12:25 PM
 */
include_once __DIR__ . '/../CacheHelper.php';

use Mockery as m;

class RoleStorageTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testGetSuperAdminRole()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('newQuery->isSuperAdmin->first')->andReturn('model');
        $this->assertEquals('model', $sut->getSuperAdminRole());
    }

    protected function getSutAndMocks()
    {
        $role_model = m::mock(\Smorken\Rbac\Models\Eloquent\Role::class);
        $sut = new \Smorken\Rbac\Storage\Eloquent\Role($role_model);
        return [$sut, $role_model];
    }

    public function testInheritsFrom()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('inheritsFrom')->andReturn('inherits');
        $this->assertEquals('inherits', $sut->inheritsFrom($model));
    }

    public function testIsSuperAdminIsBool()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('getAttribute')->with('super_admin')->andReturn(0);
        $this->assertFalse($sut->isSuperAdmin($model));
    }

    public function testDeleteWithId()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('find')->with(1)->andReturn($model);
        $model->shouldReceive('roleUsers->delete')->andReturn(2);
        $model->shouldReceive('delete')->andReturn(1);
        $this->assertEquals(1, $sut->delete(1));
    }

    public function testDeleteWithModel()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('find')->with(1)->andReturn($model);
        $model->shouldReceive('roleUsers->delete')->andReturn(2);
        $model->shouldReceive('delete')->andReturn(1);
        $this->assertEquals(1, $sut->delete($model));
    }
}
