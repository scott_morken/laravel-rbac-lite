<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 11:14 AM
 */
include_once __DIR__ . '/../CacheHelper.php';
use Mockery as m;

class RoleUserStorageTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testAdd()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('add')->with(1, 1)->andReturn($model);
        $this->assertEquals($model, $sut->add(1, 1));
    }

    protected function getSutAndMocks()
    {
        $model = m::mock(\Smorken\Rbac\Models\Eloquent\RoleUser::class);
        $role_model = m::mock(\Smorken\Rbac\Models\Eloquent\Role::class);
        $sut = new \Smorken\Rbac\Storage\Eloquent\RoleUser(['role_user' => $model, 'role' => $role_model]);
        return [$sut, $model, $role_model];
    }

    public function testRemove()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('remove')->with(1, 1)->andReturn(1);
        $this->assertEquals(1, $sut->remove(1, 1));
    }

    public function testRemoveByUserId()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('removeByUserId')->with(1)->andReturn(1);
        $this->assertEquals(1, $sut->removeByUserId(1));
    }

    public function testRemoveByRoleId()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('removeByRoleId')->with(1)->andReturn(1);
        $this->assertEquals(1, $sut->removeByRoleId(1));
    }

    public function testGetRoleUsers()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $roles = $roles->keyBy('user_id');
        $model->shouldReceive('hasUsers')->with(2)->andReturn($roles);
        $this->assertArrayHasKey(1, $sut->getRoleUsers(2));
    }

    protected function getInheritsArray()
    {
        return [
            'super-admin' => [
                'id'           => 1,
                'role_name'    => 'super-admin',
                'description'  => 'super role',
                'inherit_from' => null,
                'super_admin'  => 1,
            ],
            'admin'       => [
                'id'           => 2,
                'role_name'    => 'admin',
                'description'  => 'Admin',
                'inherit_from' => 'manage',
            ],
            'manage'      => [
                'id'           => 3,
                'role_name'    => 'manage',
                'description'  => 'Manage',
                'inherit_from' => 'user',
            ],
            'user'        => [
                'id'           => 4,
                'role_name'    => 'user',
                'description'  => 'User',
                'inherit_from' => null,
            ],
        ];
    }

    protected function mockRoleUser($user_id, $role_attrs)
    {
        $m = m::mock(\Smorken\Rbac\Contracts\Models\RoleUser::class);
        $rm = m::mock(\Smorken\Rbac\Contracts\Models\Role::class);
        $rm->id = 1;
        $rm->role_name = '';
        $rm->description = '';
        $rm->inherit_from = null;
        $rm->super_admin = false;
        foreach ($role_attrs as $k => $v) {
            $rm->$k = $v;
        }
        $m->id = 1;
        $m->role_id = $rm->id;
        $m->user_id = $user_id;
        $m->role = $rm;
        return $m;
    }

    public function testGetUserRolesNotInherited()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $user_roles = $sut->getUserRoles(1, false);
        $this->assertArrayHasKey(2, $user_roles);
        $this->assertInstanceOf(\Smorken\Rbac\Contracts\Models\Role::class, $user_roles->first());
    }

    public function testGetUserRolesInherited()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $inherited = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(1, $inherits['admin'])->role,
                $this->mockRoleUser(1, $inherits['manage'])->role,
                $this->mockRoleUser(1, $inherits['user'])->role,
            ]
        );
        $inherited = $inherited->keyBy('id');
        list($sut, $model, $rm) = $this->getSutAndMocks();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $rm->shouldReceive('inheritsFrom')->with($roles->first()->role)->andReturn($inherited);
        $r = $sut->getUserRoles(1);
        $this->assertCount(3, $r);
    }

    public function testHasRoleNotInheritedAnyRoleHasRole()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertTrue($sut->hasRole(1, '*', false));
    }

    public function testHasRoleNotInheritedNumericHasRole()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertTrue($sut->hasRole(1, 2, false));
    }

    public function testHasRoleNotInheritedRoleNameHasRole()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertTrue($sut->hasRole(1, 'admin', false));
    }

    public function testHasRoleNotInheritedAnyRoleHasNotRole()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertFalse($sut->hasRole(1, '*', false));
    }

    public function testHasRoleNotInheritedNumericHasNotRole()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertFalse($sut->hasRole(1, 3, false));
    }

    public function testHasRoleNotInheritedRoleNameHasNotRole()
    {
        $inherits = $this->getInheritsArray();
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertFalse($sut->hasRole(1, 'manage', false));
    }

    public function testHasRoleInheritedAnyRoleHasRole()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $inherited = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(1, $inherits['admin'])->role,
                $this->mockRoleUser(1, $inherits['manage'])->role,
                $this->mockRoleUser(1, $inherits['user'])->role,
            ]
        );
        $inherited = $inherited->keyBy('id');
        list($sut, $model, $rm) = $this->getSutAndMocks();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $rm->shouldReceive('inheritsFrom')->with($roles->first()->role)->andReturn($inherited);
        $this->assertTrue($sut->hasRole(1, '*'));
    }

    public function testHasRoleInheritedNumericHasRole()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $inherited = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(1, $inherits['admin'])->role,
                $this->mockRoleUser(1, $inherits['manage'])->role,
                $this->mockRoleUser(1, $inherits['user'])->role,
            ]
        );
        $inherited = $inherited->keyBy('id');
        list($sut, $model, $rm) = $this->getSutAndMocks();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $rm->shouldReceive('inheritsFrom')->with($roles->first()->role)->andReturn($inherited);
        $this->assertTrue($sut->hasRole(1, 3));
    }

    public function testHasRoleInheritedRoleNameHasRole()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $inherited = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(1, $inherits['admin'])->role,
                $this->mockRoleUser(1, $inherits['manage'])->role,
                $this->mockRoleUser(1, $inherits['user'])->role,
            ]
        );
        $inherited = $inherited->keyBy('id');
        list($sut, $model, $rm) = $this->getSutAndMocks();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $rm->shouldReceive('inheritsFrom')->with($roles->first()->role)->andReturn($inherited);
        $this->assertTrue($sut->hasRole(1, 'user'));
    }

    public function testHasRoleInheritedAnyRoleHasNotRole()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $roles = new \Illuminate\Support\Collection();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $this->assertFalse($sut->hasRole(1, '*'));
    }

    public function testHasRoleInheritedNumericHasNotRole()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $inherited = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(1, $inherits['admin'])->role,
                $this->mockRoleUser(1, $inherits['manage'])->role,
                $this->mockRoleUser(1, $inherits['user'])->role,
            ]
        );
        $inherited = $inherited->keyBy('id');
        list($sut, $model, $rm) = $this->getSutAndMocks();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $rm->shouldReceive('inheritsFrom')->with($roles->first()->role)->andReturn($inherited);
        $this->assertFalse($sut->hasRole(1, 99));
    }

    public function testHasRoleInheritedRoleNameHasNotRole()
    {
        $inherits = $this->getInheritsArray();
        unset($inherits['super-admin']);
        $roles = new \Illuminate\Support\Collection([$this->mockRoleUser(1, $inherits['admin'])]);
        $inherited = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(1, $inherits['admin'])->role,
                $this->mockRoleUser(1, $inherits['manage'])->role,
                $this->mockRoleUser(1, $inherits['user'])->role,
            ]
        );
        $inherited = $inherited->keyBy('id');
        list($sut, $model, $rm) = $this->getSutAndMocks();
        $model->shouldReceive('hasRoles')->with(1)->andReturn($roles);
        $rm->shouldReceive('inheritsFrom')->with($roles->first()->role)->andReturn($inherited);
        $this->assertFalse($sut->hasRole(1, 'fooer'));
    }
}
