<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 12:47 PM
 */
include_once __DIR__ . '/../helpers.php';

use Mockery as m;
use Smorken\Rbac\DocBlock\Parser;
use Smorken\Rbac\Rules;

class RbacMiddlewareTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testHandleWithNoRulesContinues()
    {
        list($sut, $guard, $rules, $route, $rbac) = $this->getSutAndMocks();
        $req = $this->getRequestMock($route);
        $route->shouldReceive('middleware')->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getNone'));
        $this->assertEquals('next', $sut->handle($req, $this->getClosure()));
    }

    protected function getSutAndMocks()
    {
        $parser = new Parser();
        $guard = m::mock('Illuminate\Contracts\Auth\Guard');
        $route = m::mock('Illuminate\Routing\Route');
        $store = m::mock('Smorken\Rbac\Contracts\Storage\RoleUser');
        $rules = new Rules($guard, $parser, $store);
        $sut = new \Smorken\Rbac\Middleware\Rbac($guard, $rules);
        return [$sut, $guard, $rules, $route, $store];
    }

    protected function getRequestMock($route)
    {
        return m::mock('Illuminate\Http\Request')->shouldReceive('route')->andReturn($route)->getMock();
    }

    protected function getActionName($action, $controller = null)
    {
        return sprintf('%s@%s', $controller ?: MiddlewareTestController::class, $action);
    }

    protected function getClosure()
    {
        return function () {
            return 'next';
        };
    }

    public function testHandleWithMiddlewareIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $rules, $route, $rbac) = $this->getSutAndMocks();
        $req = $this->getRequestMock($route);
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getNone'));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->once()->with(1, 'admin')->andReturn(true);
        $this->assertEquals('next', $sut->handle($req, $this->getClosure()));
    }

    public function testHandleWithMiddlewareIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $rules, $route, $rbac) = $this->getSutAndMocks();
        $req = $this->getRequestMock($route);
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getNone'));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->once()->with(1, 'admin')->andReturn(false);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('You do not have access to this resource.');
        $sut->handle($req, $this->getClosure());
    }

    public function testHandleWithTagsIsAllowed()
    {
        list($sut, $guard, $rules, $route, $rbac) = $this->getSutAndMocks();
        $req = $this->getRequestMock($route);
        $route->shouldReceive('middleware')->once()->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getAdmin'));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->once()->with(1, 'admin')->andReturn(true);
        $this->assertEquals('next', $sut->handle($req, $this->getClosure()));
    }

    public function testHandleWithTagsIsNotAllowed()
    {
        list($sut, $guard, $rules, $route, $rbac) = $this->getSutAndMocks();
        $req = $this->getRequestMock($route);
        $route->shouldReceive('middleware')->once()->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getAdmin'));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->once()->with(1, 'admin')->andReturn(false);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('You do not have access to this resource.');
        $sut->handle($req, $this->getClosure());
    }

    public function testHandleRulesRequiresAuthAndUserIsGuest()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['@'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $rules, $route, $rbac) = $this->getSutAndMocks();
        $req = $this->getRequestMock($route);
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getNone'));
        $guard->shouldReceive('check')->andReturn(false);
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('You must be authenticated to view this resource.');
        $sut->handle($req, $this->getClosure());
    }
}

class MiddlewareTestController extends \Illuminate\Routing\Controller
{

    /**
     * @rbac allow|roles:admin
     */
    public function getAdmin()
    {
    }

    public function getNone()
    {
    }
}
