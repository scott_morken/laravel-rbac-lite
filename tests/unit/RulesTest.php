<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 11:25 AM
 */

namespace unit;

use Illuminate\Routing\Controller;
use Mockery as m;
use Smorken\Rbac\DocBlock\Parser;
use Smorken\Rbac\Rules;

class RulesTest extends \PHPUnit\Framework\TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testRulesFromRouteWithNothingReturnsNull()
    {
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $this->assertNull($sut->getRulesFromRoute($route));
    }

    protected function getSutAndMocks()
    {
        $parser = new Parser();
        $guard = m::mock('Illuminate\Contracts\Auth\Guard');
        $route = m::mock('Illuminate\Routing\Route');
        $store = m::mock('Smorken\Rbac\Contracts\Storage\RoleUser');
        $sut = new Rules($guard, $parser, $store);
        return [$sut, $guard, $route, $store];
    }

    protected function getActionName($action, $controller = null)
    {
        return sprintf('%s@%s', $controller ?: TestController::class, $action);
    }

    public function testRulesFromRouteWithMiddleware()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $this->assertEquals($mw['rbac'], $sut->getRulesFromRoute($route));
    }

    public function testRulesFromRouteWithAllowTagsOnMethod()
    {
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex'));
        $expected = [
            'allow' => [
                'actions' => ['getIndex'],
                'roles'   => ['admin', 'manage'],
                'users'   => [],
            ],
            'deny'  => [
                'roles' => ['*'],
                'users' => ['*'],
            ],
        ];
        $this->assertEquals($expected, $sut->getRulesFromRoute($route));
    }

    public function testRulesFromRouteWithAllowAndDenyTagsOnMethod()
    {
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getView'));
        $expected = [
            'allow' => [
                'actions' => ['getView'],
                'roles'   => [],//default - users is checked first
                'users'   => ['*'],
            ],
            'deny'  => [
                'roles' => ['*'],
                'users' => ['2'],
            ],
        ];
        $this->assertEquals($expected, $sut->getRulesFromRoute($route));
    }

    public function testRulesFromRouteWithNoTagsOnMethodUsesControllerTags()
    {
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getAdminView'));
        $expected = [
            'allow' => [
                'actions' => ['*'],
                'roles'   => ['admin'],
                'users'   => [],
            ],
            'deny'  => [
                'roles' => ['*'],
                'users' => ['*'],
            ],
        ];
        $this->assertEquals($expected, $sut->getRulesFromRoute($route));
    }

    public function testCheckAllowedWithAllUsersRuleIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $this->assertTrue($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithAuthenticatedUsersRuleAndGuestIsNotAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['@'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(false);
        $this->assertFalse($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithAuthenticatedUsersRuleAndAuthenticatedIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['@'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user')->andReturn(true);
        $this->assertTrue($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithGuestUsersRuleAndAuthenticatedIsNotAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['?'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('guest')->andReturn(false);
        $this->assertFalse($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithGuestUsersRuleAndGuestIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['?'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('guest')->andReturn(true);
        $this->assertTrue($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithUserIdsRuleAndTruthyIsNotAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => [true, '2'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(32);
        $this->assertFalse($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithUserIdsRuleAndAuthenticatedUserIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['1', '2'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $this->assertTrue($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithUserIdsRuleAndAuthenticatedUserIsNotAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['1', '2'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(3);
        $this->assertFalse($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithAnyRoleRuleAndAuthenticatedUserWithRoleIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, '*')->andReturn(true);
        $this->assertTrue($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithAnyRoleRuleAndAuthenticatedUserWithNoRolesIsNotAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, '*')->andReturn(false);
        $this->assertFalse($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithArrayRoleRuleAndAuthenticatedUserWithMatchingRoleIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin', 'manage'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, 'admin')->andReturn(false);
        $rbac->shouldReceive('hasRole')->with(1, 'manage')->andReturn(true);
        $this->assertTrue($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckAllowedWithArrayRoleRuleAndAuthenticatedUserWithNoMatchingRoleIsNotAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin', 'manage'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, 'admin')->andReturn(false);
        $rbac->shouldReceive('hasRole')->with(1, 'manage')->andReturn(false);
        $this->assertFalse($sut->checkAllowed($mw['rbac'], $route));
    }

    public function testCheckDeniedWithAllUsersRuleIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $this->assertTrue($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithAuthenticatedUsersRuleAndGuestIsNotDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['@'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(false);
        $this->assertFalse($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithAuthenticatedUsersRuleAndAuthenticatedIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['@'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user')->andReturn(true);
        $this->assertTrue($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithGuestUsersRuleAndAuthenticatedIsNotDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['?'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('guest')->andReturn(false);
        $this->assertFalse($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithGuestUsersRuleAndGuestIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['?'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('guest')->andReturn(true);
        $this->assertTrue($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithUserIdsRuleAndTruthyIsNotDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => [true, '2'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(32);
        $this->assertFalse($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithUserIdsRuleAndAuthenticatedUserIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['1', '2'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $this->assertTrue($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithUserIdsRuleAndAuthenticatedUserIsNotDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => [],
                    'users'   => ['*'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['1', '2'],
                ],
            ],
        ];
        list($sut, $guard, $route) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(3);
        $this->assertFalse($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithAnyRoleRuleAndAuthenticatedUserWithRoleIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => [],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, '*')->andReturn(true);
        $this->assertTrue($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithAnyRoleRuleAndAuthenticatedUserWithNoRolesIsNotDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => [],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, '*')->andReturn(false);
        $this->assertFalse($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithArrayRoleRuleAndAuthenticatedUserWithMatchingRoleIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['admin', 'manage'],
                    'users' => [],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, 'admin')->andReturn(false);
        $rbac->shouldReceive('hasRole')->with(1, 'manage')->andReturn(true);
        $this->assertTrue($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckDeniedWithArrayRoleRuleAndAuthenticatedUserWithNoMatchingRoleIsNotDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['admin', 'manage'],
                    'users' => [],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->with(1, 'admin')->andReturn(false);
        $rbac->shouldReceive('hasRole')->with(1, 'manage')->andReturn(false);
        $this->assertFalse($sut->checkDenied($mw['rbac'], $route));
    }

    public function testCheckWithNoRulesIsAllowed()
    {
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->andReturnNull();
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $this->assertTrue($sut->check($route));
    }

    public function testCheckWithMiddlewareWithoutDenyCheckIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['*'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->once()->with(1, 'admin')->andReturn(true);
        $this->assertTrue($sut->check($route));
    }

    public function testCheckWithMiddlewareWithDenyCheckIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['99'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(1);
        $rbac->shouldReceive('hasRole')->once()->with(1, 'admin')->andReturn(true);
        $this->assertTrue($sut->check($route));
    }

    public function testCheckWithMiddlewareWithDenyCheckIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['admin'],
                    'users'   => [],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['99'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(99);
        $rbac->shouldReceive('hasRole')->once()->with(99, 'admin')->andReturn(true);
        $this->assertFalse($sut->check($route));
    }

    public function testCheckWithMiddlewareWithDenyCheckAndBothSpecificIsDenied()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => ['1', '2', '3', '99'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['99'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(99);
        $this->assertFalse($sut->check($route));
    }

    public function testCheckWithMiddlewareWithDenyCheckAndBothSpecificIsAllowed()
    {
        $mw = [
            'rbac' => [
                'allow' => [
                    'actions' => ['*'],
                    'roles'   => ['*'],
                    'users'   => ['1', '2', '3', '99'],
                ],
                'deny'  => [
                    'roles' => ['*'],
                    'users' => ['99'],
                ],
            ],
        ];
        list($sut, $guard, $route, $rbac) = $this->getSutAndMocks();
        $route->shouldReceive('middleware')->once()->andReturn(['rbac']);
        $route->shouldReceive('getAction')->once()->andReturn($mw);
        $route->shouldReceive('getActionName')->andReturn($this->getActionName('getIndex', NoTagsController::class));
        $guard->shouldReceive('check')->andReturn(true);
        $guard->shouldReceive('user->getAuthIdentifier')->andReturn(3);
        $this->assertTrue($sut->check($route));
    }
}

class NoTagsController extends Controller
{

    public function getIndex()
    {
    }
}

/**
 * Class TestController
 * @package unit
 *
 * @rbac allow|roles:admin
 */
class TestController extends Controller
{

    /**
     * @rbac allow|roles:admin,manage
     */
    public function getIndex()
    {
    }

    /**
     * @rbac allow|users:*
     * @rbac deny|users:2
     * @param $id
     */
    public function getView($id)
    {
    }

    public function getAdminView($id)
    {
    }
}
