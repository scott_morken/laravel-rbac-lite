<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 1:58 PM
 */

use Mockery as m;

class HandlerTest extends \PHPUnit\Framework\TestCase
{

    public function testAll()
    {
        list($sut, $auth, $rup, $rp) = $this->getSutAndMocks();
        $c = new \Illuminate\Support\Collection();
        $rp->shouldReceive('all')->andReturn($c);
        $this->assertEquals($c, $sut->all());
    }

    public function testHasRoleNoUserId()
    {
        $list = $this->getInheritsArray();
        $roles = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(2, $list['admin'])->role,
                $this->mockRoleUser(2, $list['manage'])->role,
                $this->mockRoleUser(2, $list['user'])->role,
            ]
        );
        list($sut, $auth, $rup, $rp) = $this->getSutAndMocks();
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $rup->shouldReceive('getUserRoles')->with(2)->andReturn($roles);
        $rp->shouldReceive('isSuperAdmin')->andReturn(false);
        $rup->shouldReceive('hasRole')->with(2, 1, true)->andReturn(true);
        $this->assertTrue($sut->hasRole(1));
    }

    protected function getInheritsArray()
    {
        return [
            'super-admin' => [
                'id'           => 1,
                'role_name'    => 'super-admin',
                'description'  => 'super role',
                'inherit_from' => null,
                'super_admin'  => 1,
            ],
            'admin'       => [
                'id'           => 2,
                'role_name'    => 'admin',
                'description'  => 'Admin',
                'inherit_from' => 'manage',
            ],
            'manage'      => [
                'id'           => 3,
                'role_name'    => 'manage',
                'description'  => 'Manage',
                'inherit_from' => 'user',
            ],
            'user'        => [
                'id'           => 4,
                'role_name'    => 'user',
                'description'  => 'User',
                'inherit_from' => null,
            ],
        ];
    }

    protected function mockRoleUser($user_id, $role_attrs)
    {
        $m = m::mock(\Smorken\Rbac\Contracts\Models\RoleUser::class);
        $rm = m::mock(\Smorken\Rbac\Contracts\Models\Role::class);
        $rm->id = 1;
        $rm->role_name = '';
        $rm->description = '';
        $rm->inherit_from = null;
        $rm->super_admin = false;
        foreach ($role_attrs as $k => $v) {
            $rm->$k = $v;
        }
        $m->id = 1;
        $m->role_id = $rm->id;
        $m->user_id = $user_id;
        $m->role = $rm;
        return $m;
    }

    protected function getSutAndMocks()
    {
        $auth = m::mock('Illuminate\Contracts\Auth\Guard');
        $rp = m::mock('Smorken\Rbac\Contracts\Storage\Role');
        $rup = m::mock('Smorken\Rbac\Contracts\Storage\RoleUser');
        $sut = new \Smorken\Rbac\Handler($auth, $rup, $rp);
        return [$sut, $auth, $rup, $rp];
    }

    public function testHasRoleWithUserId()
    {
        $list = $this->getInheritsArray();
        $roles = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(2, $list['admin'])->role,
                $this->mockRoleUser(2, $list['manage'])->role,
                $this->mockRoleUser(2, $list['user'])->role,
            ]
        );
        list($sut, $auth, $rup, $rp) = $this->getSutAndMocks();
        $rup->shouldReceive('getUserRoles')->with(2)->andReturn($roles);
        $rp->shouldReceive('isSuperAdmin')->andReturn(false);
        $rup->shouldReceive('hasRole')->with(2, 1, true)->andReturn(true);
        $this->assertTrue($sut->hasRole(1, true, 2));
    }

    public function testHasRoleWithUserIdIsSuperAdmin()
    {
        $list = $this->getInheritsArray();
        $roles = new \Illuminate\Support\Collection(
            [
                $this->mockRoleUser(2, $list['super-admin'])->role,
                $this->mockRoleUser(2, $list['manage'])->role,
                $this->mockRoleUser(2, $list['user'])->role,
            ]
        );
        list($sut, $auth, $rup, $rp) = $this->getSutAndMocks();
        $rup->shouldReceive('getUserRoles')->with(2)->andReturn($roles);
        $rp->shouldReceive('isSuperAdmin')->andReturn(true);
        $rup->shouldReceive('hasRole')->never();
        $this->assertTrue($sut->hasRole(99, true, 2));
    }
}
