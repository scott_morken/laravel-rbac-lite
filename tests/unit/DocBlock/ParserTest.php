<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 9:33 AM
 */
namespace tests\unit\DocBlock;

use Smorken\Rbac\DocBlock\Parser;

class ParserTest extends \PHPUnit\Framework\TestCase
{

    public function testSetReflectorThrowsExceptionWithInvalidArg()
    {
        $this->expectException(\InvalidArgumentException::class);
        $sut = new Parser();
        $sut->setReflector(123);
    }

    public function testSetReflectorWithValidClassString()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass');
        $this->assertInstanceOf(\Reflector::class, $sut->getReflector());
    }

    public function testSetReflectorWithClassAndMethodStringDefault()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass::myMethod');
        $this->assertInstanceOf(\Reflector::class, $sut->getReflector());
    }

    public function testSetReflectorWithClassAndMethodStringActionStyle()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass@myMethod');
        $this->assertInstanceOf(\Reflector::class, $sut->getReflector());
    }

    public function testSetReflectorWithArray()
    {
        $sut = new Parser(['\tests\unit\DocBlock\RefClass', 'myMethod']);
        $this->assertInstanceOf(\Reflector::class, $sut->getReflector());
    }

    public function testSetReflectorWithReflector()
    {
        $r = new \ReflectionFunction(
            function () {
                return 'foo';
            }
        );
        $sut = new Parser($r);
        $this->assertInstanceOf(\Reflector::class, $sut->getReflector());
    }

    public function testGetDocBlockWithNoBlockOnMethodIsFalse()
    {
        $sut = new Parser(['\tests\unit\DocBlock\RefClass', 'noBlock']);
        $db = $sut->getDocBlock();
        $this->assertEmpty($db);
    }

    public function testGetDocBlockWithBlockOnMethod()
    {
        $sut = new Parser(['\tests\unit\DocBlock\RefClass', 'myMethod']);
        $db = $sut->getDocBlock();
        $expected = [
            'Something',
            '@method myMethod',
            '@foo this is foo',
            '@foo hello this foo has bad spacing',
        ];
        $this->assertEquals($expected, $db);
    }

    public function testGetDocBlockWithBlockOnClass()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass');
        $db = $sut->getDocBlock();
        $expected = [
            'Class RefClass',
            '@package tests\unit\DocBlock',
            '@foo tag',
            '@foo tag one two three',
        ];
        $this->assertEquals($expected, $db);
    }

    public function testGetTagFromClass()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass');
        $tags = $sut->getTag('foo');
        $expected = [
            2 => 'tag',
            3 => 'tag one two three',
        ];
        $this->assertEquals($expected, $tags);
    }

    public function testGetTagFromMethod()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass::myMethod');
        $tags = $sut->getTag('foo');
        $expected = [
            2 => 'this is foo',
            3 => 'hello this foo has bad spacing',
        ];
        $this->assertEquals($expected, $tags);
    }

    public function testGetTagFromMethodWithAllowedStar()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass::hasStar');
        $tags = $sut->getTag('foo');
        $expected = [
            'this has a *',
        ];
        $this->assertEquals($expected, $tags);
    }

    public function testGetTagFromMethodWithNoTagsIsEmpty()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass::noBlock');
        $tags = $sut->getTag('foo');
        $expected = [];
        $this->assertEquals($expected, $tags);
    }

    public function testGetTagNotExistingEmpty()
    {
        $sut = new Parser('\tests\unit\DocBlock\RefClass::noBlock');
        $tags = $sut->getTag('bar');
        $expected = [];
        $this->assertEquals($expected, $tags);
    }
}

/**
 * Class RefClass
 * @package tests\unit\DocBlock
 *
 * @foo tag
 * @foo  tag one   two     three
 */
class RefClass
{

    /**
     * Something
     *
     * @method myMethod
     * @foo this is foo
     * @foo   hello   this foo    has bad   spacing
     */
    public function myMethod()
    {
    }

    public function noBlock()
    {
    }

    /**
     * @foo this has a *
     */
    public function hasStar()
    {
    }
}
