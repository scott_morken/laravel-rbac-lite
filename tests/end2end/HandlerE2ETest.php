<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 7:16 AM
 */
include_once __DIR__ . '/Setup/Resolver.php';
include_once __DIR__ . '/Setup/CreateRbacTablesForTest.php';
include_once __DIR__ . '/../unit/CacheHelper.php';

use end2end\Setup\CreateRbacTablesForTest;
use Mockery as m;

class HandlerE2ETest extends \PHPUnit\Framework\TestCase
{

    protected $resolver;

    public function setUp()
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        $this->setupDB();
    }
    public function tearDown()
    {
        m::close();
        $this->resolver = null;
        \end2end\Setup\Resolver::reset();
    }

    public function testHasRoleNoUserIdAndNumericRoleIsTrue()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(2, 1);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertTrue($sut->hasRole(1));
    }

    public function testHasRoleNoUserIdAndNumericRoleIsFalse()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertFalse($sut->hasRole(1));
    }

    public function testHasRoleNoUserIdAndRoleNameIsTrue()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(2, 2);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertTrue($sut->hasRole('admin'));
    }

    public function testHasRoleNoUserIdAndRoleNameIsFalse()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertFalse($sut->hasRole('admin'));
    }

    public function testHasRoleNoUserIdAndInheritedRoleNameIsTrue()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(2, 2);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertTrue($sut->hasRole('manage'));
    }

    public function testHasRoleNoUserIdAndNotInheritedRoleNameIsFalse()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(2, 2);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertFalse($sut->hasRole('manage', false));
    }

    public function testHasRoleNoUserIdAndSuperAdminWithInheritedRoleNameIsTrue()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(2, 1);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(2);
        $this->assertTrue($sut->hasRole('manage'));
        $this->assertTrue($sut->isSuperAdmin());
    }

    public function testHasRoleWithUserIdAndNumericRoleIsTrue()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(99, 1);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(99);
        $this->assertTrue($sut->hasRole(1, true, 99));
    }

    public function testHasRoleWithUserIdAndNumericRoleIsFalse()
    {
        list($sut, $auth, $role_user) = $this->getSutAndMocks();
        $role_user->add(99, 1);
        $auth->shouldReceive('check')->andReturn(true);
        $auth->shouldReceive('user->getAuthIdentifier')->andReturn(10);
        $this->assertFalse($sut->hasRole(1, true, 10));
    }

    protected function getSutAndMocks()
    {
        $auth = m::mock('Illuminate\Contracts\Auth\Guard');
        $rm = $this->getModel(\Smorken\Rbac\Models\Eloquent\Role::class);
        $rum = $this->getModel(\Smorken\Rbac\Models\Eloquent\RoleUser::class);
        $role_user = new \Smorken\Rbac\Storage\Eloquent\RoleUser(['role_user' => $rum, 'role' => $rm]);
        $role = new \Smorken\Rbac\Storage\Eloquent\Role($rm);
        $sut = new \Smorken\Rbac\Handler($auth, $role_user, $role);
        return [$sut, $auth, $role_user, $role];
    }
    protected function getModel($model_class, $attrs = [])
    {
        $model = new $model_class($attrs);
        forward_static_call([$model_class, 'setConnectionResolver'], $this->getConnectionResolver());
        return $model;
    }

    protected function getConnectionResolver()
    {
        if (!$this->resolver) {
            $this->resolver = \end2end\Setup\Resolver::get();
        }
        return $this->resolver;
    }

    protected function setupDB()
    {
        $m = new CreateRbacTablesForTest();
        $m->up();
        $rm = $this->getModel(\Smorken\Rbac\Models\Eloquent\Role::class);
        $m->seed($rm);
    }
}
