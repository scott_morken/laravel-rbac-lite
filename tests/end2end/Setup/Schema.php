<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 7:39 AM
 */

include_once __DIR__ . '/Resolver.php';

use end2end\Setup\Resolver;

class Schema extends \Illuminate\Support\Facades\Schema
{

    /**
     * Get a schema builder instance for a connection.
     *
     * @param  string $name
     * @return \Illuminate\Database\Schema\Builder
     */
    public static function connection($name)
    {
        return Resolver::get()->connection($name)->getSchemaBuilder();
    }

    /**
     * Get a schema builder instance for the default connection.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    protected static function getFacadeAccessor()
    {
        return Resolver::get()->connection()->getSchemaBuilder();
    }
}
