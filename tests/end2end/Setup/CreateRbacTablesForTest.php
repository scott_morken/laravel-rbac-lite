<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 7:39 AM
 */

namespace end2end\Setup;

use Smorken\Rbac\Models\Eloquent\Role;

include_once __DIR__ . '/Schema.php';

class CreateRbacTablesForTest extends \CreateRbacTables
{

    protected $connection = 'test';

    public function seed(Role $role_model)
    {
        $role_model->create(
            [
                'role_name'   => 'super_admin',
                'description' => 'Super Admin',
                'super_admin' => true,
            ]
        );
        $role_model->create(
            [
                'role_name'    => 'admin',
                'description'  => 'Admin',
                'inherit_from' => 'manage',
            ]
        );
        $role_model->create(
            [
                'role_name'    => 'manage',
                'description'  => 'Manager',
                'inherit_from' => 'user',
            ]
        );
        $role_model->create(
            [
                'role_name'   => 'user',
                'description' => 'User',
            ]
        );
    }

}
