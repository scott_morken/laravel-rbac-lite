<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 7:27 AM
 */
return [
    'master' => 'smorken/views::layouts.rightops',
    'roles'         => [
        'name'  => 'Smorken\Rbac\Contracts\Storage\Role',
        'impl'  => \Smorken\Rbac\Storage\Eloquent\Role::class,
        'model' => \Smorken\Rbac\Models\Eloquent\Role::class,
    ],
    'role_user'     => [
        'name'  => 'Smorken\Rbac\Contracts\Storage\RoleUser',
        'impl'  => \Smorken\Rbac\Storage\Eloquent\RoleUser::class,
        'model' => [
            'role_user' => \Smorken\Rbac\Models\Eloquent\RoleUser::class,
            'role'      => \Smorken\Rbac\Models\Eloquent\Role::class,
        ],
    ],
    'allowed_roles' => ['admin'],
    'rules'         => [
        'code'    => 403,
        'message' => 'You do not have access to this resource.',
    ],
];
