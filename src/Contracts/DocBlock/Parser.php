<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 9:16 AM
 */

namespace Smorken\Rbac\Contracts\DocBlock;

interface Parser
{

    /**
     * @param string|array|\Reflector $reflector
     * @return Parser
     */
    public function setReflector($reflector);

    /**
     * @return \Reflector
     * @throws \OutOfBoundsException
     */
    public function getReflector();

    /**
     * @return string
     */
    public function getDocBlock();

    /**
     * @param string $tag
     * @return array
     */
    public function getTag($tag);

    /**
     * @param string $tag
     * @param string $docblock
     * @return array
     */
    public function getTagFromDocBlock($tag, $docblock);
}
