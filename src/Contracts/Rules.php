<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:22 AM
 */

namespace Smorken\Rbac\Contracts;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Routing\Route;
use Smorken\Rbac\Contracts\DocBlock\Parser;
use Smorken\Rbac\Contracts\Storage\RoleUser;
use Smorken\Rbac\Exceptions\RbacException;

interface Rules
{

    /**
     * @return Guard
     */
    public function getAuth();

    /**
     * @param Guard $auth
     * @return null
     */
    public function setAuth(Guard $auth);

    /**
     * @return Parser
     */
    public function getParser();

    /**
     * @param Parser $parser
     * @return null
     */
    public function setParser(Parser $parser);

    /**
     * @return RoleUser
     */
    public function getRbac();

    /**
     * @param RoleUser $rbac
     * @return mixed
     */
    public function setRbac(RoleUser $rbac);

    /**
     * @param Route $route
     * @param array $rules
     * @return bool
     * @throws RbacException
     */
    public function check(Route $route, array $rules = []);

    /**
     * @param array $rules
     * @param Route $route
     * @return bool
     */
    public function checkAllowed(array $rules, Route $route);

    /**
     * @param array $rules
     * @param Route $route
     * @return bool
     */
    public function checkDenied(array $rules, Route $route);

    /**
     * @param Route $route
     * @return array|null
     */
    public function getRulesFromRoute(Route $route);

    /**
     * @return int
     */
    public function getCode();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param int $code
     * @return null
     */
    public function setCode($code);

    /**
     * @param string $message
     * @return null
     */
    public function setMessage($message);
}
