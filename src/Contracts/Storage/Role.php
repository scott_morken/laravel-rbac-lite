<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:13 PM
 */

namespace Smorken\Rbac\Contracts\Storage;

interface Role
{

    /**
     * @return \Smorken\Rbac\Contracts\Models\Role
     */
    public function getSuperAdminRole();

    /**
     * @param \Smorken\Rbac\Contracts\Models\Role $model
     * @return bool
     */
    public function isSuperAdmin(\Smorken\Rbac\Contracts\Models\Role $model);

    /**
     * @param \Smorken\Rbac\Contracts\Models\Role $model
     * @return \Smorken\Rbac\Contracts\Models\Role[]
     */
    public function inheritsFrom(\Smorken\Rbac\Contracts\Models\Role $model);
}
