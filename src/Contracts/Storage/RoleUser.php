<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:13 PM
 */

namespace Smorken\Rbac\Contracts\Storage;

interface RoleUser
{

    public function add($user_id, $role_id);

    public function remove($user_id, $role_id);

    public function removeByUserId($user_id);

    public function removeByRoleId($role_id);

    public function hasRole($user_id, $role_id, $inherited = true);

    public function getUserRoles($user_id, $inherited = true);

    public function getRoleUsers($role_id);

    /**
     * @return \Smorken\Rbac\Contracts\Models\Role
     */
    public function getRoleModel();
}
