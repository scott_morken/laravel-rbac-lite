<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 1:33 PM
 */

namespace Smorken\Rbac\Contracts;

use Illuminate\Contracts\Auth\Guard;
use Smorken\Rbac\Contracts\Storage\Role;
use Smorken\Rbac\Contracts\Storage\RoleUser;

interface Handler
{

    /**
     * @param Guard $auth
     * @return null
     */
    public function setAuth(Guard $auth);

    /**
     * @return Guard
     */
    public function getAuth();

    /**
     * @return Role
     */
    public function getRoleProvider();

    /**
     * @param Role $role
     * @return null
     */
    public function setRoleProvider(Role $role);

    /**
     * @return RoleUser
     */
    public function getRoleUserProvider();

    /**
     * @param RoleUser $roleUser
     * @return null
     */
    public function setRoleUserProvider(RoleUser $roleUser);

    /**
     * @return \Smorken\Rbac\Contracts\Models\Role[]
     */
    public function all();

    /**
     * @param int|string $role_id
     * @param bool $inherited
     * @param null $user_id
     * @return bool
     */
    public function hasRole($role_id, $inherited = true, $user_id = null);

    /**
     * @param null $user_id
     * @param bool $inherited
     * @return Models\Role[]
     */
    public function getRoles($user_id = null, $inherited = true);

    /**
     * @param null $user_id
     * @return bool
     */
    public function isSuperAdmin($user_id = null);
}
