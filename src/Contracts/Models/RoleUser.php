<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:15 PM
 */

namespace Smorken\Rbac\Contracts\Models;

/**
 * Interface RoleUser
 * @package Smorken\Rbac\Contracts\Models
 *
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 *
 * @property Role $role
 */
interface RoleUser
{

    public function add($user_id, $role_id);

    public function remove($user_id, $role_id);

    public function removeByUserId($user_id);

    public function removeByRoleId($role_id);

    public function hasRole($user_id, $role_id);

    public function hasRoles($user_id);

    public function hasUsers($role_id);
}
