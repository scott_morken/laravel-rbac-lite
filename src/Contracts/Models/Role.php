<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:14 PM
 */

namespace Smorken\Rbac\Contracts\Models;

use Illuminate\Support\Collection;

/**
 * Interface Role
 * @package Smorken\Rbac\Contracts\Models
 *
 * @property int $id
 * @property string $role_name
 * @property string $description
 * @property string $inherit_from
 * @property bool $super_admin
 * @property bool $inherited
 *
 * @property RoleUser[] $roleUsers
 */
interface Role
{

    public function inheritsFrom($model = null, Collection $inherits = null);

    public function loadInheritsFrom($name);
}
