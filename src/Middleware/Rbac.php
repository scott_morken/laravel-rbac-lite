<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 12:44 PM
 */

namespace Smorken\Rbac\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Smorken\Rbac\Contracts\Rules;

class Rbac
{

    /**
     * @var Rules
     */
    protected $rules;

    /**
     * @var Guard
     */
    protected $auth;

    public function __construct(Guard $auth, Rules $rules)
    {
        $this->rules = $rules;
        $this->auth = $auth;
    }

    public function handle($request, \Closure $next)
    {
        $route = $request->route();
        if (!$this->rules->check($route)) {
            abort($this->rules->getCode(), $this->rules->getMessage());
        }
        return $next($request);
    }
}
