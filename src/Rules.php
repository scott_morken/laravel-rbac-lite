<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:33 AM
 */

namespace Smorken\Rbac;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Routing\Route;
use Smorken\Rbac\Contracts\DocBlock\Parser;
use Smorken\Rbac\Contracts\Storage\RoleUser;
use Smorken\Rbac\Exceptions\RbacException;

class Rules implements \Smorken\Rbac\Contracts\Rules
{

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var RoleUser
     */
    protected $rbac;

    /**
     * @var Route
     */
    protected $route;

    protected $code = 403;

    protected $message = 'You do not have access to this resource.';

    public function __construct(Guard $auth, Parser $parser, RoleUser $rbac)
    {
        $this->setAuth($auth);
        $this->setParser($parser);
        $this->setRbac($rbac);
    }

    /**
     * @param Route $route
     * @param array $rules
     * @return bool
     */
    public function check(Route $route, array $rules = [])
    {
        if (empty($rules)) {
            $rules = $this->getRulesFromRoute($route);
        }
        if (is_array($rules) && count($rules)) {
            $allowed = $this->checkAllowed($rules, $route);
            if ($this->shouldCheckDenied($allowed, $rules)) {
                $allowed = !$this->checkDenied($rules, $route);
            }
            return $allowed;
        }
        return true;
    }

    /**
     * @param Route $route
     * @return mixed|null
     */
    public function getRulesFromRoute(Route $route)
    {
        $rules = null;
        $mw = $route->middleware();
        if ($mw && in_array('rbac', $mw)) {
            $rules = array_get($route->getAction(), 'rbac', null);
        }
        if (is_null($rules)) {
            $rules = $this->getRulesFromRouteTags($route);
        }
        return $rules;
    }

    protected function getRulesFromRouteTags(Route $route)
    {
        $full_action = $route->getActionName();
        $ctrl = $this->getControllerNameFromRoute($route);
        $rules = $this->parseRulesFromTags(
            $this->getParser()->setReflector($full_action)->getTag('rbac'),
            $this->getActionNameFromRoute($route)
        );
        if (!$rules) {
            $rules = $this->parseRulesFromTags($this->getParser()->setReflector($ctrl)->getTag('rbac'));
        }
        return $rules;
    }

    protected function getControllerNameFromRoute(Route $route)
    {
        $parts = $this->getActionPartsFromRoute($route);
        return head($parts);
    }

    protected function getActionPartsFromRoute(Route $route)
    {
        $full_action = $route->getActionName();
        $parts = explode('@', $full_action);
        return $parts;
    }

    protected function parseRulesFromTags($tags, $action = false)
    {
        if ($tags) {
            $ruleset = $this->getDefaultRuleset($action);
            foreach ($tags as $tag) {
                $parsed = $this->parseTagForRuleType($tag);
                $ruleset = $this->mergeRulesIntoRuleset($parsed, $ruleset);
            }
            return $ruleset;
        }
        return null;
    }

    protected function getDefaultRuleset($action)
    {
        return [
            'allow' => [
                'actions' => [$action ?: '*'],
                'roles'   => [],
                'users'   => [],
            ],
            'deny'  => [
                'roles' => ['*'],
                'users' => ['*'],
            ],
        ];
    }

    /**
     * Rule: allow|users:1,2,3
     * Rule: allow|roles:admin,manage
     * Rule: allow|roles:*
     * Rule: deny|roles:manage
     * Rule: deny|roles:*
     * @param array $tag
     * @return array
     */
    protected function parseTagForRuleType($tag)
    {
        $parts = explode('|', $tag);
        $type = head($parts);
        $rules = last($parts);
        return [$type => $this->parseRulesFromRuleString($rules)];
    }

    /**
     * @param $rule_string
     * @return array
     */
    protected function parseRulesFromRuleString($rule_string)
    {
        $allowed = ['roles', 'users'];
        $parts = explode(':', $rule_string);
        $key = head($parts);
        if (!in_array($key, $allowed)) {
            throw new \InvalidArgumentException("$key is not an allowed RBAC rule.");
        }
        $options = array_map(
            function ($v) {
                return trim($v);
            },
            explode(',', last($parts))
        );
        return [$key => $options];
    }

    protected function mergeRulesIntoRuleset($new, $ruleset)
    {
        $can_recurse = ['allow', 'deny'];
        foreach ($new as $k => $v) {
            if ($v) {
                if (in_array($k, $can_recurse) && array_key_exists($k, $ruleset)) {
                    $ruleset[$k] = $this->mergeRulesIntoRuleset($v, $ruleset[$k]);
                } else {
                    $ruleset[$k] = $v;
                }
            }
        }
        return $ruleset;
    }

    /**
     * @return Parser
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @param Parser $parser
     * @return null
     */
    public function setParser(Parser $parser)
    {
        $this->parser = $parser;
    }

    protected function getActionNameFromRoute(Route $route)
    {
        $parts = $this->getActionPartsFromRoute($route);
        return last($parts);
    }

    /**
     * @param array $rules
     * @param Route $route
     * @return bool
     */
    public function checkAllowed(array $rules, Route $route)
    {
        $allowed = null;
        $allow_rules = array_get($rules, 'allow', null);
        if ($allow_rules) {
            $allowed = $this->checkRuleset($allow_rules, $route);
        }
        return $allowed;
    }

    protected function checkRuleset(array $rules, Route $route)
    {
        $check = false;
        $action = $this->getActionNameFromRoute($route);
        $route_rules = $this->getRulesetForAction($rules, $action);
        if ($route_rules) {
            $check = $this->checkRulesetIsMet($route_rules);
        }
        return $check;
    }

    protected function getRulesetForAction($rules, $action)
    {
        $actions = null;
        if (isset($rules['actions'])) {
            $actions = (array)$rules['actions'];
        } else {
            $actions = '*';
        }
        if ($actions === '*' || (is_array($actions) && in_array('*', $actions))) {
            return $rules;
        }
        if ($actions && $actions === $action ||
            (
                is_array($actions) && in_array(
                    $action,
                    $actions
                )
            )
        ) {
            return $rules;
        }
        return null;
    }

    protected function checkRulesetIsMet($rules)
    {
        if (isset($rules['users']) && $rules['users']) {
            return $this->checkUsers($rules['users']);
        }
        if (isset($rules['roles']) && $rules['roles']) {
            return $this->checkRoles($rules['roles']);
        }
        throw new RbacException('No rules specified even though access control was requested.');
    }

    protected function checkUsers($users)
    {
        $users = (array)$users;
        if (($auth_type = $this->isAuthType($users)) !== false) {
            return $this->checkAuthType($auth_type);
        }
        $user_id = $this->getUserId();
        if ($user_id) {
            return $this->compareUserIds($user_id, $users);
        }
        return false;
    }

    protected function isAuthType($items)
    {
        if (is_string($items)) {
            return in_array($items, $this->getAuthTypes()) ? $items : false;
        }
        if (is_array($items)) {
            foreach ($items as $item) {
                $r = $this->isAuthType($item);
                if ($r !== false) {
                    return $r;
                }
            }
        }
        return false;
    }

    /**
     * * - any user
     * ? - guest user
     * @ - authenticated user
     * @return array
     */
    protected function getAuthTypes()
    {
        return ['*', '?', '@'];
    }

    /**
     * $type is one of '*', '?', '@'
     * * - any user, guest or authenticated
     * ? - guest user
     * @ - authenticated user
     * @param $type
     * @throws RbacException
     * @return bool
     */
    protected function checkAuthType($type)
    {
        $has_type = $this->checkAuthTypeAllowed($type);
        if (!$has_type && $type == '@') {
            $this->setCode(401);
            $this->setMessage('You must be authenticated to view this resource.');
        }
        return $has_type;
    }

    /**
     * $type is one of '*', '?', '@'
     * * - any user, guest or authenticated
     * ? - guest user
     * @ - authenticated user
     * @param $type
     * @throws RbacException
     * @return bool
     */
    protected function checkAuthTypeAllowed($type)
    {
        switch ($type) {
            case '*':
                $has_type = true;
                break;
            case '?':
                $has_type = $this->getAuth()->guest();
                break;
            case '@':
                $has_type = $this->getAuth()->check() && $this->getAuth()->user();
                break;
            default:
                throw new RbacException(
                    "Unexpected user type [$type]. Allowed: " . implode(', ', $this->getAuthTypes())
                );
        }
        return $has_type;
    }

    /**
     * @return Guard
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param Guard $auth
     * @return null
     */
    public function setAuth(Guard $auth)
    {
        $this->auth = $auth;
    }

    protected function getUserId()
    {
        return $this->getAuth()->check() ? $this->getAuth()->user()->getAuthIdentifier() : null;
    }

    protected function compareUserIds($user_id, $users)
    {
        foreach ($users as $id) {
            if ((string)$user_id === (string)$id) {
                return true;
            }
        }
        return false;
    }

    protected function checkRoles($roles)
    {
        $roles = (array)$roles;
        foreach ($roles as $role) {
            if ($this->checkRole($role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $role
     * @return bool
     */
    protected function checkRole($role)
    {
        return $this->getRbac()->hasRole($this->getUserId(), $role);
    }

    /**
     * @return RoleUser
     */
    public function getRbac()
    {
        return $this->rbac;
    }

    /**
     * @param RoleUser $rbac
     * @return mixed
     */
    public function setRbac(RoleUser $rbac)
    {
        $this->rbac = $rbac;
    }

    protected function shouldCheckDenied($allowed, array $rules)
    {
        return $allowed === null || $this->checkRuleSpecificityNeedsDeny($rules);
    }

    protected function checkRuleSpecificityNeedsDeny(array $rules)
    {
        $denied = array_get($rules, 'deny', null);
        $allowed = array_get($rules, 'allow', null);
        if (is_null($denied)) {
            return false;
        }
        if (is_null($allowed)) {
            return true;
        }
        $allow_users = array_get($allowed, 'users', []);
        $deny_users = array_get($denied, 'users', []);
        if ($this->checkRuleIsMoreSpecific($allow_users, $deny_users)) {
            return true;
        }
        $allow_roles = array_get($allowed, 'roles', []);
        $deny_roles = array_get($denied, 'roles', []);
        return $this->checkRuleIsMoreSpecific($allow_roles, $deny_roles);
    }

    protected function checkRuleIsMoreSpecific($allow, $deny)
    {
        $allow_is_low = $this->isAuthType($allow) || empty($allow);
        $deny_is_low = $this->isAuthType($deny) || empty($deny);
        return ($allow_is_low && !$deny_is_low) || (!$allow_is_low && !$deny_is_low); //!$deny_is_low would work
    }

    /**
     * @param array $rules
     * @param Route $route
     * @return bool
     */
    public function checkDenied(array $rules, Route $route)
    {
        $denied = true;
        $deny_rules = array_get($rules, 'deny', null);
        if ($deny_rules) {
            $denied = $this->checkRuleset($deny_rules, $route);
        }
        return $denied;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return null
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return null
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
