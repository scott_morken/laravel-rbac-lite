<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 12:06 PM
 */

namespace Smorken\Rbac\Console\Commands;

use Illuminate\Console\Command;
use Smorken\Rbac\Contracts\Storage\Role as RoleProvider;
use Smorken\Rbac\Contracts\Storage\RoleUser;

class Role extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:role {user_id : User ID} 
    {role_id? : Override the role ID, default is 1}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registers a user to a role (default super admin)';

    /**
     * @var RoleUser
     */
    protected $roleUserProvider;

    /**
     * @var RoleProvider
     */
    protected $roleProvider;

    public function __construct(RoleUser $roleUserProvider, RoleProvider $roleProvider)
    {
        $this->roleUserProvider = $roleUserProvider;
        $this->roleProvider = $roleProvider;
        parent::__construct();
    }

    public function handle()
    {
        $user_id = $this->argument('user_id');
        $role = $this->getRole();
        if (!$role) {
            $this->error('No super admin role found. Did you seed the tables?');
            return 1;
        }
        $this->roleUserProvider->add($user_id, $role->id);
        if ($this->roleUserProvider->errors()) {
            $this->error('Unable to add user to role.');
            foreach ($this->roleUserProvider->errors()->all() as $msg) {
                $this->info($msg);
            }
        } else {
            $this->info("User [$user_id] added to role {$role}");
        }
    }

    protected function getRole()
    {
        $role_id = $this->argument('role_id');
        if (!$role_id) {
            return $this->roleProvider->getSuperAdminRole();
        }
        return $this->roleProvider->find($role_id);
    }
}
