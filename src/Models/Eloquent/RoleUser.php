<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 2:10 PM
 */

namespace Smorken\Rbac\Models\Eloquent;

class RoleUser extends Base implements \Smorken\Rbac\Contracts\Models\RoleUser
{

    protected $fillable = ['role_id', 'user_id'];

    protected $rules = [
        'role_id' => 'required|int',
        'user_id' => 'required|int',
    ];

    protected $table = 'role_user';

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function add($user_id, $role_id)
    {
        return $this->newQuery()->firstOrCreate(['user_id' => $user_id, 'role_id' => $role_id]);
    }

    public function remove($user_id, $role_id)
    {
        return $this->newQuery()->where('user_id', $user_id)->roleIdIs($role_id)->delete();
    }

    public function removeByUserId($user_id)
    {
        return $this->newQuery()->where('user_id', $user_id)->delete();
    }

    public function removeByRoleId($role_id)
    {
        return $this->newQuery()->roleIdIs($role_id)->delete();
    }

    public function hasRole($user_id, $role_id)
    {
        return $this->newQuery()->roleIdIs($role_id)->with('role')->where('user_id', $user_id)->first();
    }

    public function hasRoles($user_id)
    {
        return $this->newQuery()->where('user_id', $user_id)->with('role')->get()->keyBy('role_id');
    }

    public function hasUsers($role_id)
    {
        return $this->newQuery()->roleIdIs($role_id)->with('role')->get()->keyBy('user_id');
    }

    public function scopeRoleIdIs($query, $role_id)
    {
        if (is_numeric($role_id)) {
            $query->where('role_id', $role_id);
        } else {
            $query->whereHas(
                'role',
                function ($q) use ($role_id) {
                    $q->where('role_name', $role_id);
                }
            );
        }
        return $query;
    }
}
