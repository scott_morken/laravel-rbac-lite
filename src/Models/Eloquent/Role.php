<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 1:47 PM
 */

namespace Smorken\Rbac\Models\Eloquent;

use Illuminate\Support\Collection;

class Role extends Base implements \Smorken\Rbac\Contracts\Models\Role
{

    protected $fillable = ['role_name', 'description', 'inherit_from', 'super_admin'];

    protected $rules = [
        'role_name'    => 'required|max:16',
        'description'  => 'required|max:64',
        'inherit_from' => 'max:16',
        'super_admin'  => 'boolean',
    ];

    protected $inherited = false;

    public function __toString()
    {
        return sprintf('[%s] %s', $this->role_name, $this->description);
    }

    public function roleUsers()
    {
        return $this->hasMany(RoleUser::class);
    }

    public function setRoleNameAttribute($value)
    {
        array_set($this->attributes, 'role_name', str_slug($value));
    }

    public function setInheritedAttribute($value)
    {
        $this->inherited = $value;
    }

    public function getInheritedAttribute()
    {
        return $this->inherited;
    }

    public function scopeIsSuperAdmin($query, $is = 1)
    {
        $query->where('super_admin', $is);
        return $query;
    }

    public function inheritsFrom($model = null, Collection $inherits = null)
    {
        if (is_null($model)) {
            $model = $this;
        }
        if (is_null($inherits)) {
            $inherits = new Collection();
        }
        $key = 'rbac/inherits/' . $model->role_name;
        $self = $this;
        return \Cache::remember(
            $key,
            60 * 4,
            function () use ($self, $model, $inherits) {
                if ($model->super_admin) {
                    return $self->inheritsSuperAdmin($model);
                } else {
                    return $self->inheritsRecursion($model, $inherits);
                }
            }
        );
    }

    protected function inheritsSuperAdmin($model)
    {
        $results = $this->newQuery()->isSuperAdmin(0)->get()->keyBy('id');
        $inherited = $results->each(
            function ($item, $key) {
                $item->inherited = true;
            }
        );
        $inherited->put($model->id, $model);
        return $inherited;
    }

    protected function inheritsRecursion($model, $inherits)
    {
        $inherits->put($model->id, $model);
        if ($model->inherit_from) {
            $new_model = $this->loadInheritsFrom($model->inherit_from);
            if ($new_model) {
                $new_model->inherited = true;
                return $this->inheritsRecursion($new_model, $inherits);
            }
        }
        return $inherits;
    }

    public function loadInheritsFrom($name)
    {
        return $this->newQuery()
                    ->where('role_name', $name)
                    ->first();
    }
}
