<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:03 AM
 */

namespace Smorken\Rbac\Http\Controllers;

use Illuminate\Http\Request;
use Smorken\Ext\Controller\FullController;
use Smorken\Rbac\Contracts\Storage\Role;
use Smorken\Rbac\Http\Requests\RoleForm;

class RoleController extends FullController
{

    protected $base = 'role';

    protected $subnav = 'admin';

    protected $package = 'smorken/rbac::';

    public function __construct(Role $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function postSave(Request $r, $id = null)
    {
        app(RoleForm::class);
        return $this->doSave($r, $id);
    }

    /**
     * Setup the master template.
     *
     * @return void
     */
    protected function setupMaster()
    {
        if (!$this->master) {
            $this->master = config('smorken/rbac::config.master', 'smorken/views::layouts.rightops');
        }
        if ($this->master) {
            view()->share('master', $this->master);
        }
    }

    protected function getData(Request $r)
    {
        return $r->all();
    }
}
