<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 9:00 AM
 */

namespace Smorken\Rbac\Http\Requests;

use Smorken\Ext\Http\Requests\Request;
use Smorken\Rbac\Contracts\Storage\Role;
use Smorken\Sanitizer\Contracts\Sanitize;

class RoleForm extends Request
{

    public function rules(Sanitize $s, Role $provider)
    {
        return $this->sanitizeAndGetRules($s, $provider);
    }

    /**
     * @param array $input
     * @param Sanitize $s
     * @return array
     */
    protected function doSanitize($input, Sanitize $s)
    {
        $input['role_name'] = $s->alphaNumDashSpace($input['role_name']);
        $input['description'] = $s->string($input['description']);
        $input['inherit_from'] = $s->alphaNumDashSpace($input['inherit_from']);
        $input['super_admin'] = $s->bool($input['super_admin']);
        return $input;
    }
}
