<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 9:18 AM
 */

namespace Smorken\Rbac\DocBlock;

class Parser implements \Smorken\Rbac\Contracts\DocBlock\Parser
{

    /**
     * @var \Reflector
     */
    protected $reflector;

    public function __construct($reflector = null)
    {
        if (!is_null($reflector)) {
            $this->setReflector($reflector);
        }
    }

    /**
     * @param string $tag
     * @return array
     */
    public function getTag($tag)
    {
        return $this->getTagFromDocBlock($tag, $this->getDocBlock());
    }

    /**
     * @param string $tag
     * @param string $docblock
     * @return array
     */
    public function getTagFromDocBlock($tag, $docblock)
    {
        if (is_string($docblock)) {
            $docblock = $this->parseDocBlockToArray($docblock);
        }
        $regex = sprintf('/@%s\s*(.*)/', $tag);
        $matches = [];
        $count = 0;
        $replaced = preg_replace($regex, '$1', $docblock, -1, $count);
        if ($count) {
            $matches = array_diff($replaced, $docblock);
        }
        return $matches;
    }

    protected function parseDocBlockToArray($docblock)
    {
        $docarray = [];
        if ($docblock !== false) {
            $docblock = $this->cleanDocBlock($docblock);
            $t = explode(PHP_EOL, $docblock);
            foreach ($t as $i => $v) {
                $v = trim($v);
                if ($v) {
                    $docarray[] = trim($v);
                }
            }
        }
        return $docarray;
    }

    protected function cleanDocBlock($docblock)
    {
        $docblock = preg_replace('#^\s*[\/*]+#m', '', $docblock);
        return trim(preg_replace('#[\t\r\f ]{2,}#', ' ', $docblock));
    }

    /**
     * @return string
     */
    public function getDocBlock()
    {
        $docblock = $this->getReflector()->getDocComment();
        return $this->parseDocBlockToArray($docblock);
    }

    /**
     * @return \Reflector
     * @throws \OutOfBoundsException
     */
    public function getReflector()
    {
        if (!$this->reflector) {
            throw new \OutOfBoundsException('A reflector must be provided.');
        }
        return $this->reflector;
    }

    /**
     * @param \Reflector|string|array $reflector
     * @return $this
     */
    public function setReflector($reflector)
    {
        if (is_string($reflector)) {
            $this->reflector = $this->getReflectorFromString($reflector);
        }
        if (is_array($reflector)) {
            $this->reflector = new \ReflectionMethod($reflector[0], $reflector[1]);
        }
        if ($reflector instanceof \Reflector) {
            $this->reflector = $reflector;
        }
        if (!$this->reflector) {
            throw new \InvalidArgumentException('Reflector did not match the expected criteria.');
        }
        return $this;
    }

    protected function getReflectorFromString($reflector)
    {
        if (stripos($reflector, '@')) {
            $reflector = str_replace('@', '::', $reflector);
        }
        if (stripos($reflector, '::')) {
            return new \ReflectionMethod($reflector);
        }
        return new \ReflectionClass($reflector);
    }
}
