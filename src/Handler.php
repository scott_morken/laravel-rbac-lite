<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 1:33 PM
 */

namespace Smorken\Rbac;

use Illuminate\Contracts\Auth\Guard;
use Smorken\Rbac\Contracts\Models;
use Smorken\Rbac\Contracts\Storage\Role;
use Smorken\Rbac\Contracts\Storage\RoleUser;

class Handler implements \Smorken\Rbac\Contracts\Handler
{

    protected $auth;

    protected $role_provider;

    protected $role_user_provider;

    /**
     * Handler constructor.
     * @param Guard $auth
     * @param RoleUser $roleUser
     * @param Role $role
     */
    public function __construct(Guard $auth, RoleUser $roleUser, Role $role)
    {
        $this->setAuth($auth);
        $this->setRoleUserProvider($roleUser);
        $this->setRoleProvider($role);
    }

    /**
     * @param Guard $auth
     * @return null
     */
    public function setAuth(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @return Guard
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @return Role
     */
    public function getRoleProvider()
    {
        return $this->role_provider;
    }

    /**
     * @param Role $role
     * @return null
     */
    public function setRoleProvider(Role $role)
    {
        $this->role_provider = $role;
    }

    /**
     * @return RoleUser
     */
    public function getRoleUserProvider()
    {
        return $this->role_user_provider;
    }

    /**
     * @param RoleUser $roleUser
     * @return null
     */
    public function setRoleUserProvider(RoleUser $roleUser)
    {
        $this->role_user_provider = $roleUser;
    }

    public function all()
    {
        $criteria = [
            'orderBy' => [
                ['id']
            ]
        ];
        return $this->getRoleProvider()->all($criteria);
    }

    /**
     * @param int|string $role_id
     * @param bool $inherited
     * @param null $user_id
     * @return bool
     */
    public function hasRole($role_id, $inherited = true, $user_id = null)
    {
        $user_id = $this->getUserId($user_id);
        if ($this->isSuperAdmin($user_id) && $inherited) {
            return true;
        }
        return $this->getRoleUserProvider()->hasRole($user_id, $role_id, $inherited);
    }

    /**
     * @param null $user_id
     * @param bool $inherited
     * @return Models\Role[]
     */
    public function getRoles($user_id = null, $inherited = true)
    {
        $user_id = $this->getUserId($user_id);
        return $this->getRoleUserProvider()->getUserRoles($user_id);
    }

    /**
     * @param null $user_id
     * @return bool
     */
    public function isSuperAdmin($user_id = null)
    {
        $roles = $this->getRoles($user_id);
        foreach ($roles as $id => $role) {
            if ($this->getRoleProvider()->isSuperAdmin($role)) {
                return true;
            }
        }
        return false;
    }

    protected function getUserId($user_id)
    {
        if (is_null($user_id)) {
            $user_id = $this->getAuth()->check() ? $this->getAuth()->user()->getAuthIdentifier() : null;
        }
        return $user_id;
    }
}
