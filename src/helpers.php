<?php
if (!function_exists('rbac')) {
    function rbac($role_id = null)
    {
        $rbac = app('rbac');
        if (!is_null($role_id)) {
            return $rbac->hasRole($role_id);
        }
        return $rbac;
    }
}
