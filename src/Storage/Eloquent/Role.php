<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 2:21 PM
 */

namespace Smorken\Rbac\Storage\Eloquent;

class Role extends Base implements \Smorken\Rbac\Contracts\Storage\Role
{

    /**
     * @return \Smorken\Rbac\Contracts\Models\Role
     */
    public function getSuperAdminRole()
    {
        return $this->getModel()->newQuery()->isSuperAdmin()->first();
    }

    /**
     * @param \Smorken\Rbac\Contracts\Models\Role $model
     * @return \Smorken\Rbac\Contracts\Models\Role[]
     */
    public function inheritsFrom(\Smorken\Rbac\Contracts\Models\Role $model)
    {
        return $model->inheritsFrom();
    }

    /**
     * @param \Smorken\Rbac\Contracts\Models\Role $model
     * @return bool
     */
    public function isSuperAdmin(\Smorken\Rbac\Contracts\Models\Role $model)
    {
        return (bool)$model->super_admin;
    }

    public function delete($model)
    {
        if (!is_object($model)) {
            $model = $this->getModel()->find($model);
        }
        if ($model) {
            $model->roleUsers()->delete();
            $result = $model->delete();
            \Cache::flush();
            return $result;
        }
        return false;
    }
}
