<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:12 AM
 */

namespace Smorken\Rbac\Storage\Eloquent;

use Illuminate\Support\Collection;
use Smorken\Rbac\Exceptions\RbacException;

class RoleUser extends Base implements \Smorken\Rbac\Contracts\Storage\RoleUser
{

    public function add($user_id, $role_id)
    {
        $result = $this->getModel()->add($user_id, $role_id);
        \Cache::flush();
        return $result;
    }

    /**
     * @return \Smorken\Rbac\Contracts\Models\RoleUser
     * @throws RbacException
     */
    public function getModel()
    {
        if (!is_array($this->model) || !isset($this->model['role_user'])) {
            throw new RbacException('Role User requires both role_user and role models.');
        }
        return $this->model['role_user'];
    }

    public function remove($user_id, $role_id)
    {
        $result = $this->getModel()->remove($user_id, $role_id);
        \Cache::flush();
        return $result;
    }

    public function removeByUserId($user_id)
    {
        $result = $this->getModel()->removeByUserId($user_id);
        \Cache::flush();
        return $result;
    }

    public function removeByRoleId($role_id)
    {
        $result = $this->getModel()->removeByRoleId($role_id);
        \Cache::flush();
        return $result;
    }

    public function hasRole($user_id, $role_id, $inherited = true)
    {
        $roles = $this->getUserRoles($user_id, $inherited);
        if ($role_id === '*' && count($roles)) {
            return true;
        }
        if (is_numeric($role_id)) {
            return $roles->get($role_id) !== null;
        }
        return $roles->first(
            function ($value, $key) use ($role_id) {
                return $value->role_name === $role_id;
            }
        ) !== null;
    }

    public function getUserRoles($user_id, $inherited = true)
    {
        if (!$inherited) {
            return $this->getUserRolesNoInheritance($user_id);
        }
        return $this->getUserRolesInherited($user_id);
    }

    protected function getUserRolesNoInheritance($user_id)
    {
        $key = 'rbac/roles/base/' . $user_id;
        $self = $this;
        return \Cache::remember(
            $key,
            60 * 4,
            function () use ($self, $user_id) {
                return $self->convertRoleUserToRole($self->getModel()->hasRoles($user_id));
            }
        );
    }

    protected function convertRoleUserToRole($models)
    {
        $coll = new Collection();
        foreach ($models as $model) {
            $coll->put($model->role_id, $model->role);
        }
        return $coll;
    }

    protected function getUserRolesInherited($user_id)
    {
        $key = 'rbac/roles/inherited/' . $user_id;
        $self = $this;
        return \Cache::remember(
            $key,
            60 * 4,
            function () use ($self, $user_id) {
                $user_roles = $self->convertRoleUserToRole($self->getModel()->hasRoles($user_id));
                return $self->buildInheritanceList($user_roles);
            }
        );
    }

    /**
     * @param \Smorken\Rbac\Contracts\Models\Role[] $user_roles
     * @return Collection
     */
    protected function buildInheritanceList($user_roles)
    {
        $inherited = new Collection();
        foreach ($user_roles as $user_role) {
            $role = $this->getRoleModel();
            $inh_roles = $role->inheritsFrom($user_role);
            $inherited = $inherited->merge($inh_roles);
        }
        return $inherited->keyBy('id');
    }

    /**
     * @return \Smorken\Rbac\Contracts\Models\Role
     * @throws RbacException
     */
    public function getRoleModel()
    {
        if (!is_array($this->model) || !isset($this->model['role'])) {
            throw new RbacException('Role User requires both role_user and role models.');
        }
        return $this->model['role'];
    }

    public function getRoleUsers($role_id)
    {
        return $this->getModel()->hasUsers($role_id);
    }
}
