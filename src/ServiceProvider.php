<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:04 AM
 */

namespace Smorken\Rbac;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Routing\Router;
use Smorken\Rbac\DocBlock\Parser;
use Smorken\Rbac\Middleware\Rbac;
use Smorken\Storage\Contracts\Binder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot(Router $router)
    {
        $this->app->make('Illuminate\Database\Eloquent\Factory')->load(__DIR__ . '/../database/factories');
        $this->loadMigrationsFrom( __DIR__ . '/../database/migrations');
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/rbac');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path('/resources/views/vendor/smorken/rbac'),
            ],
            'views'
        );
        $router->middleware('rbac', Rbac::class);
    }

    protected function bootConfig()
    {
        $package_path = __DIR__ . '/../config/config.php';
        $app_path = config_path('rbac-config.php');
        $this->mergeConfigWith($package_path, $app_path, 'smorken/rbac::config');
        $this->publishes([$package_path => $app_path], 'config');
    }

    protected function mergeConfigWith($package_path, $app_path, $key)
    {
        if (file_exists($app_path)) {
            $app_config = require $app_path;
        } else {
            $app_config = [];
        }
        $package_config = require $package_path;
        $this->app['config']->set($key, array_merge($package_config, $app_config));
    }

    public function register()
    {
        $this->bootConfig();
        $config = $this->app['config']->get('smorken/rbac::config', []);
        $binder = $this->getBinder();
        $this->bindProvider(array_get($config, 'roles', []), $binder);
        $this->bindProvider(array_get($config, 'role_user', []), $binder);
        $this->bindRules($config);
        $this->bindRbac($config);
    }

    protected function getBinder()
    {
        if (!$this->app->bound(Binder::class)) {
            $this->app->register(\Smorken\Storage\ServiceProvider::class);
        }
        return $this->app[Binder::class];
    }

    protected function bindRbac($config)
    {
        $this->app->bind(
            \Smorken\Rbac\Contracts\Handler::class,
            function ($app) use ($config) {
                $role_user_class = array_get($config, 'role_user.name', null);
                $role_class = array_get($config, 'roles.name', null);
                return new Handler($app[Guard::class], $app[$role_user_class], $app[$role_class]);
            }
        );
        $this->app->alias(\Smorken\Rbac\Contracts\Handler::class, 'rbac');
    }

    protected function bindRules($config)
    {
        $this->app->bind(
            \Smorken\Rbac\Contracts\Rules::class,
            function ($app) use ($config) {
                $auth = $app[Guard::class];
                $parser = new Parser();
                $role_user_class = array_get($config, 'role_user.name', null);
                $role_user = $app[$role_user_class];
                $rules = new Rules($auth, $parser, $role_user);
                $rules->setCode(array_get($config, 'rules.code', '403'));
                $rules->setMessage(array_get($config, 'rules.message', 'You do not have access to this resources.'));
                return $rules;
            }
        );
        $this->app->alias(\Smorken\Rbac\Contracts\Rules::class, 'rbac.rules');
    }

    protected function bindProvider($config, Binder $binder)
    {
        $contract = array_get($config, 'name');
        $impl = array_get($config, 'impl');
        $model = array_get($config, 'model');
        $binder->bindName($contract, $impl, $model);
    }
}
